
```bash
axios 0.21.0 0.27.2
dotenv 10.0.0 16.0.3

fastify-static: 4.2.0 4.7.0
fastify: 2.14.1 3.6.0
node-fetch: 3.0.0 3.2.10
```
